package me.gt3ch1.main;

import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

import me.gt3ch1.events.PlayerWalkEvent;

public class Main extends JavaPlugin {
	
	@Override
	public void onEnable() {
		getLogger().log(Level.INFO, ChatColor.RED + "[[FastPaths]] Enabled");
		Bukkit.getPluginManager().registerEvents(new PlayerWalkEvent(), this);
	}
	
	@Override 
	public void onDisable() {
		getLogger().log(Level.INFO, ChatColor.BLUE + "[[FastPaths]] Disabled");

	}

}
